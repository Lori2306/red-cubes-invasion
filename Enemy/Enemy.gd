extends RigidBody

export var DESPAWN_TIME = 5

var rng = RandomNumberGenerator.new()

export(Array, AudioStream) var audio_effects

onready var despawn_timer = $DespawnTimer

func _ready():
	set_sound()
	despawn_timer.wait_time = DESPAWN_TIME
	
func _physics_process(_delta):
	apply_central_impulse(0.2*global_transform.basis.z)

func set_sound():
	rng.randomize()
	$EnemySoundEffect.stream = audio_effects[rng.randi_range(0,2)]
	$EnemySoundEffect.playing = true
	$EnemySoundEffect.stream.set_loop_mode(AudioStreamSample.LOOP_FORWARD)

func _on_DespawnTimer_timeout():
	queue_free()

func enemy_hit():
	queue_free()




func _on_DeathTimer_timeout():
	queue_free()
