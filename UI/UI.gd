extends Control

var score : int = 0
var livello : int = 1
var vite : int = 0
export var DEFAULT_TEXT_PUNTEGGIO : String
export var DEFAULT_TEXT_LIVELLO : String
export var DEFAULT_TEXT_CUORI : String

func _ready():
	$MarginContainer/VBoxContainer/LevelUp.set_visible(false)
	$MarginContainer/VBoxContainer/Score.set_visible(false)
	$MarginContainer/VBoxContainer/Hit.set_visible(false)
	$MarginContainer/VBoxContainer/GameOver.set_visible(false)
	set_text()
	
func set_text():
	$MarginContainer/VBoxContainer/Punteggio.text = DEFAULT_TEXT_PUNTEGGIO + String(score)
	$MarginContainer/VBoxContainer/Livello.text = DEFAULT_TEXT_LIVELLO + String(livello)
	$MarginContainer/VBoxContainer/Cuori.text = DEFAULT_TEXT_CUORI + String(vite)

func _on_Main_set_level(new_level):
	show_output($MarginContainer/VBoxContainer/LevelUp, "LEVEL UP!!",1, 0.8)
	livello = new_level
	set_text()

func _on_Main_set_score(new_score):
	show_output($MarginContainer/VBoxContainer/Score, "SCORE!!",2 , 0.2)
	score = new_score
	set_text()	
	
func _on_Main_set_life(new_life):
	vite = new_life
	show_output($MarginContainer/VBoxContainer/Hit, "HIT!!",3, 0.2)
	set_text()

func show_output(ob, frase : String, suffix, wait : float):
	ob.text = frase
	ob.set_visible(true)
	ob.get_node("Timer" + String(suffix)).wait_time = wait
	ob.get_node("Timer" + String(suffix)).start()

func _on_Timer2_timeout():
	$MarginContainer/VBoxContainer/Score.set_visible(false)


func _on_Timer3_timeout():
	$MarginContainer/VBoxContainer/Hit.set_visible(false)


func _on_Timer1_timeout():
	$MarginContainer/VBoxContainer/LevelUp.set_visible(false)


func _on_Main_gameover():
	$MarginContainer/VBoxContainer/GameOver.set_visible(true)






