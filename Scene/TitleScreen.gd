extends Control

onready var main_scene = "res://Scene/Main.tscn"

func _ready():
	$Audio/Music.playing = true

func _physics_process(_delta):
	if $Audio/Music.playing == false:
		$Audio/Music.playing = true
	if Input.is_action_pressed("Start"):
			get_tree().change_scene(main_scene)
