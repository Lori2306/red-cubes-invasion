extends Spatial

export(Vector3) var DEFAULT_ENEMY_POSITION
export var MAX_HEALTH = 5

var score : int
var level: int
var vite : int

const DIFFICULTY_FACTOR : float = 0.2
const DEFAULT_TIME : float = 5.0

enum {PLAY, LEVELUP, HIT, GAMEOVER, RESTART, NEXTSTAGE}
var state : int
var has_level_up : bool

signal gameover
signal set_score(new_score)
signal set_level(new_level)
signal set_life(new_life)


var rng = RandomNumberGenerator.new()
onready var timer : Timer = $Timer/EnemySpawnTimer
onready var enemy = preload("res://Enemy/Enemy.tscn") 
onready var main_scene = "res://Scene/Main.tscn"

func _ready():
	has_level_up = false
	DEFAULT_ENEMY_POSITION = $EnemiesPosition.transform.origin
	state = PLAY
	score = 0
	level = 1
	vite = MAX_HEALTH
	emit_signal("set_level", level)
	emit_signal("set_life", vite)
	emit_signal("set_score", score)
	set_difficulty()

func _physics_process(_delta):
	match state:
		PLAY:
			if(score % 10 == 0 and score > 0):
				if(not has_level_up):
					state = LEVELUP
					has_level_up = true
				if(level>=5):
					state = NEXTSTAGE
			if vite == 0:
				state = GAMEOVER
		NEXTSTAGE:
			state = GAMEOVER
		LEVELUP:
			set_difficulty()
			level = score/10 + 1
			emit_signal("set_level", level)
			state = PLAY
		GAMEOVER:
			gameover()
		RESTART:
			if Input.is_action_pressed("Start"):
				get_tree().change_scene(main_scene)

func set_score():
	has_level_up = false
	score = score+1
	emit_signal("set_score",score)

func _on_Timer_timeout():
	if(state == PLAY):
		var e = spawn(enemy, $Enemies)
		e.transform.origin = Vector3(rng.randi_range(-10, 10) , DEFAULT_ENEMY_POSITION.y, DEFAULT_ENEMY_POSITION.z)
	
func spawn(nodeToSpawn, parentNode:Node):
	var o = nodeToSpawn.instance()
	parentNode.add_child(o)
	set_difficulty()
	return o

func set_difficulty():
	rng.randomize()
	timer.wait_time = clamp(rng.randf_range(0.01, DEFAULT_TIME*(1-DIFFICULTY_FACTOR*level)), 0.01, DEFAULT_TIME)

func gameover():
	emit_signal("gameover")
	state = RESTART

func _on_Area_body_entered(body):
	if body.is_in_group("Enemies"):
		vite=vite-1
		emit_signal("set_life", vite)
