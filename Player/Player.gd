extends KinematicBody

export var TURNSPEED : float = 0.1
export var STRAFESPEED : int = 10
export var SHOOT_TIME : float = 0.3
export var is_alive : bool = true
export(Array, AudioStream) var audio_effects

onready var legs = $Ben/Legs
onready var shoot_timer = $ShootTimer
onready var bullet = preload("res://Player/Bullet.tscn")

var can_shoot = true
var rng = RandomNumberGenerator.new()

func _ready():
	rng.randomize()
	shoot_timer.wait_time = SHOOT_TIME
	pass 

func _physics_process(_delta):
	if(is_alive):
		var direction = Vector3()
		if Input.is_action_pressed("StrafeRight"):
			direction = transform.basis.x;
		if Input.is_action_pressed("StrafeLeft"):
			direction = -transform.basis.x
		if Input.is_action_pressed("Shoot"):
			if(can_shoot):
				can_shoot=false
				shoot()
		turn(-direction.x*TURNSPEED)
		move(direction.normalized()*STRAFESPEED)


func move(velocity : Vector3):
	move_and_slide(velocity,Vector3.UP)
	var origin = transform.origin
	origin.x = clamp(origin.x, -10, 10)
	transform.origin = origin
	
func turn(angle : float):
	legs.rotate_y(angle)
	var legs_rotation = legs.rotation_degrees
	legs_rotation.y = clamp(legs_rotation.y, -90, 90)
	legs.rotation_degrees = legs_rotation

func shoot():
	generate_sound()
	shoot_timer.start()
	var b : RigidBody = get_parent().get_parent().spawn(bullet, self)
	b.global_transform.origin = $Ben/Head/turret/Muzzle.global_transform.origin

func generate_sound():
	$ShootEffect.stream = audio_effects[rng.randi_range(0,audio_effects.size())-1]
	$ShootEffect.playing = true

func _on_Main_gameover():
	is_alive = false


func _on_ShootTimer_timeout():
	can_shoot=true
	
