extends RigidBody

export var BULLET_SPEED : float = 20

func _ready():
	$Particles.emitting = true
	set_as_toplevel(true)

func _physics_process(_delta):
	apply_central_impulse(-transform.basis.z)


func _on_DespawnTimer_timeout():
	queue_free()


func _on_Area_body_entered(body):
	if body.is_in_group("Enemies"):
		body.enemy_hit()
		get_parent().get_parent().get_parent().set_score()
	explode()

func explode():
	queue_free()
